import React, { Component } from 'react'
import Fade from 'react-reveal/Fade';
export class AboutMe extends Component {
    render() {
        return (


            <section className=" section static ">

                <div className="container mb-4">
                    <Fade >
                        <div className="text-center">
                            <h2 className="font-weight-bold mb-0 display-4">About me</h2>

                        </div>


                        <div className=" text-justify mx-2">

                            <p className=" float-left">
                                Hi there, I'm Luis a computer science student who enjoys designing and
                                code simple but impactful user experiences. I understand that the perfect
                                 User Interface should look great and work even better, so I'm always willing
                                 to take risks and I'm not afraid to make mistakes if I can learn from them.</p>

                        </div>
                    </Fade >


                </div>

                <div className="container mt-4c">

                    <div className="row">

                        <Fade >
                            <div className="col-md-3 col-sm-4" data-sal="slide-up" data-sal-duration="800">
                                <h2 className="font-weight-bold mb-4 ">Principles</h2>
                            </div>
                        </Fade>

                        <div className="col-md-9 col-sm-8" data-sal="slide-up" data-sal-duration="800">
                            <div className="row  row-cols-1 row-cols-md-2">
                                <Fade bottom>
                                    <div className="col mb-4">
                                        <h5 className="font-weight-bold mb-3">UX is the sum of all</h5>
                                        <p>There is not style over usability, I bring style and functionality together to
                                        create something that looks beautiful and performs ideally.</p>
                                    </div>
                                </Fade>
                                <Fade bottom>
                                    <div className="col mb-4">
                                        <h5 className="font-weight-bold mb-3">Accidents often produce the best solutions.</h5>
                                        <p>Things don’t always go to plan, and the ideas are far
                                            from linear. As a team player, I'm always looking for the best idea to come into
                                        the table, and I'm not afraid to make mistakes if I can learn from them.</p>
                                    </div>
                                </Fade>
                                <Fade bottom>
                                    <div className="col mb-4">
                                        <h5 className="font-weight-bold mb-3">Simplicity is the soul of efficiency.</h5>
                                        <p>Creative, minimalistic, and simple, That's what defines me. Clean designs, always
                                            trying to minimize the complexity of my code to write it in a cleaner way. I
                                            believe in looking for the simplest solution for complex problems its what makes
                                        the difference.</p>
                                    </div>
                                </Fade>
                                <Fade bottom>
                                    <div className="col mb-4">
                                        <h5 className="font-weight-bold mb-3">None of us is as smart as all of us.</h5>
                                        <p>Teamwork is the ability to work together toward a common vision. It is the
                                        fuel that allows common people to attain uncommon results.</p>
                                    </div>
                                </Fade>



                            </div>

                        </div>

                    </div>
                </div>

            </section>


        )
    }
}

export default AboutMe
