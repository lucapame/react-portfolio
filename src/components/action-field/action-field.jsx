import React, { Component } from 'react'
import Fade from 'react-reveal/Fade';

export class ActionField extends Component {
    render() {
        return (
            <section className=" section  parallax bg2">

                <div className="align-content-around">

                    <div className="container ">
                        <div className="row">
                            <div className="col-md-9 col-sm-8" >
                                <div className="row  row-cols-1 row-cols-md-2">
                                    <Fade bottom>
                                        <div className="col mb-4">
                                            <h5 className="font-weight-bold my-2 h3">Web Apps & Web Sites</h5>
                                            <p>This is where visuals and problem-solving work together to deliver an efficient, good looking
                                             product.</p>

                                        </div>
                                    </Fade>
                                    <Fade bottom>
                                        <div className="col mb-4">
                                            <h5 className="font-weight-bold my-2 h3">Mobile App Design</h5>
                                            <p>Using the correct tools and an innovative vision I can take any idea from a concept into
                                               reality</p>
                                        </div>
                                    </Fade>
                                    <Fade bottom>
                                        <div className="col mb-4">
                                            <h5 className="font-weight-bold my-2 h3">UI/UX Design</h5>
                                            <p>I personally love this topic, I enjoy making a product easy and atractive for the user.</p>
                                        </div>
                                    </Fade>
                                </div>
                            </div>
                            <Fade >
                                <div className="col-md-3 col-sm-4" >
                                    <h2 className="font-weight-bold mb-4 ">What I do?</h2>
                                </div>
                            </Fade>

                        </div>
                    </div>
                    <div className="container text-center">
                        <i className=" m-4 fab fa-js fa-4x"></i>
                        <i className=" m-4 fab fa-angular fa-4x"></i>
                        <i className=" m-4 fab fa-react fa-4x"></i>
                        <i className=" m-4 fab fa-node fa-4x"></i>
                        <i className=" m-4 fab fa-adobe fa-4x"></i>
                    </div>
                </div>


            </section>
        )
    }
}

export default ActionField;
