import React, { Component } from 'react'
import './header.scss'
import Fade from 'react-reveal/Fade';

export class Header extends Component {
    render() {
        return (

            <div className="header  parallax bg1 d-flex align-items-center ">
                <div className="container ">
                    <div>
                        <Fade bottom>
                            <div className="">
                                <h1 className="display-1 mb-0 font-weight-bold"> Hello! <br />
                                    Luis Parra here.</h1>
                            </div>
                        </Fade>
                    </div>

                    <div className="text-white"><i className="fab fa-github px-1 fa-2x"></i>
                        <i className="fab fa-linkedin px-1 "></i></div>
                </div>
            </div>


        )
    }
}

export default Header;
