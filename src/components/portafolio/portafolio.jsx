import React, { Component } from 'react'
import img1 from "../../assets/img/pk-img.png";
import Fade from 'react-reveal/Fade';

export class Portafolio extends Component {
    render() {
        return (
            <div>
                <section className=" section static ">

                    <div className="container">
                        <Fade>
                            <div class="row">
                                <div className="col-4">
                                    <div className="card card-visibility text-white border-0">
                                        <img src={img1} alt="..." className="card-img" />

                                        <div className="card-img-overlay">

                                            <div className="card-content p-2">
                                                <h5 className="card-title">Logistics Project</h5>
                                                <p className="card-text">This is a small project developed in the Intensive Training Program in Prokarma Mexico</p>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Fade>
                    </div>



                </section>

            </div>
        )
    }
}

export default Portafolio;
