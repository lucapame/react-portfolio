import React, { Component } from 'react'
import './home.scss'
import { Link } from 'react-router-dom'
import Header from '../header/header';
import AboutMe from '../about-me/about-me';
import ActionField from '../action-field/action-field';
import Portafolio from '../portafolio/portafolio';


export class Home extends Component {
    render() {
        return (
            <div className="wrapper">
                {/* <div className="sticky-top  nav-custom d-flex justify-content-between" data-sal="slide-up" data-sal-duration="800">
                    <img src="/img/logo.svg" width="40" height="40" className="d-relative" alt=""></img>
                    <ul className="nav justify-content-end  pr-4 pt-3">
                        <li className="nav-item ">

                            <Link to="/aboutme" className="nav-link ">About Me</Link>
                        </li>
                        <li className="nav-item ">
                            <Link to="/contact" className="nav-link ">Contact</Link>
                        </li>
                    </ul>
                </div> */}

                <Header />

                <AboutMe />
                <div className="spacer-xl d-block  d-md-none"></div>
                <ActionField />
                <div className="spacer-xl d-block  d-md-none"></div>
                <Portafolio />

                <div className="container text-center mb-4" >
                    <p className="stroke h3">
                        Luis Carlos Parra Mendoza </p>
                </div>

            </div>
        )
    }
}

export default Home;
