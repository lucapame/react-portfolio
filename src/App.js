import React, { Fragment } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { RouterOutlet } from 'react-router-outlet'
import './styles/styles.scss'
import Home from "./components/home/home";
import '../node_modules/@fortawesome/fontawesome-free/css/all.css'
const App = () => {



  const routes = [
    {
      path: '/home',
      component: Home
    },
    {
      path: '/',
      exact: true,
      redirectTo: '/home'
    },
  ];

  return (

    <Fragment>
      <Router>
        <RouterOutlet routes={routes} />
      </Router>
    </Fragment>
  );
};

export default App;
